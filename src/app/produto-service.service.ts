import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Pages } from './pages';
import { Produto } from './produto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  constructor(private http:HttpClient) { }

  getProducts():Observable<Produto[]>{
    return this.http.get<Produto[]>(`${environment.apiUrl}api/v1/products`)
  }

}
