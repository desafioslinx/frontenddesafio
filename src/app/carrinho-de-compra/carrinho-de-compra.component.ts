import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Carrinho } from '../carrinho';

@Component({
  selector: 'app-carrinho-de-compra',
  templateUrl: './carrinho-de-compra.component.html',
  styleUrls: ['./carrinho-de-compra.component.css']
})
export class CarrinhoDeCompraComponent implements OnInit {

  cart: Carrinho

  constructor(private _cartService: CartService) { }

  ngOnInit(): void {
    this.iniciarCarrinho()
  }

  iniciarCarrinho() {
    this._cartService.getCart().subscribe(response => {
      this.cart = response[0]
    })
  }

}
