import { Component, OnInit } from '@angular/core';
import { Produto } from '../produto';
import { ProdutoItem } from '../itemProduto';
import { ProdutoService } from '../produto-service.service';
import { CartService } from '../cart.service';
import { Carrinho } from '../carrinho';

@Component({
  selector: 'app-produto-item',
  templateUrl: './produto-item.component.html',
  styleUrls: ['./produto-item.component.css']
})
export class ProdutoItemComponent implements OnInit {

  produtos: Produto[] = []

  produtoItem: ProdutoItem

  cart: Carrinho

  constructor(private produtoService: ProdutoService, private _cartService: CartService) { }

  ngOnInit(): void {
    this.iniciarProdutos()
    this.iniciarCarrinho()
  }

  iniciarCarrinho() {
    this._cartService.getCart().subscribe(response => {
      this.cart = response[0]
    })
  }

  adicionarCarrinho(produto: Produto) {
    this.produtoItem = { product: produto, quantidade: 1, precoTotal: 0 }    
    this.produtoItem.cart = this.cart
    this._cartService.addCart(this.produtoItem).subscribe()
  }

  iniciarProdutos() {
    this.produtoService.getProducts().subscribe(response => {
      this.produtos = response
    })
  }
}
