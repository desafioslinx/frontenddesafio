import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListagemProdutoComponent } from './listagem-produto/listagem-produto.component';
import { ProdutoItemComponent } from './produto-item/produto-item.component';
import { CarrinhoDeCompraComponent } from './carrinho-de-compra/carrinho-de-compra.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    ListagemProdutoComponent,
    ProdutoItemComponent,
    CarrinhoDeCompraComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
