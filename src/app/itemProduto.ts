import { Produto } from './produto';
import { Carrinho } from './carrinho';

export interface ProdutoItem{
    product:Produto
    quantidade:number
    precoTotal:number;
    cart?:Carrinho;
}