export interface Produto{
    id:number
    name:string
    description:string
    price:number
    urlImage:string
}