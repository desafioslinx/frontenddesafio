import { ProdutoItem } from './itemProduto';

export interface Carrinho{
    id:number;
    totalPrice:number;
    productItens:ProdutoItem[]
}