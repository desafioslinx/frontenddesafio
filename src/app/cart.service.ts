import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProdutoItem } from './itemProduto';
import { environment } from 'src/environments/environment';
import { Carrinho } from './carrinho';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  
  constructor(private http:HttpClient) { }
  
  addCart(produtoItem:ProdutoItem){
    return this.http.post<ProdutoItem>(`${environment.apiUrl}api/v1/carts/`,produtoItem)
  }
  
  getCart() {    
    return this.http.get<Carrinho>(`${environment.apiUrl}api/v1/carts`);
  }
}
