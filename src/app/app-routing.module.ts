import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListagemProdutoComponent } from './listagem-produto/listagem-produto.component';
import { CarrinhoDeCompraComponent } from './carrinho-de-compra/carrinho-de-compra.component';


const routes: Routes = [
  { path: '', component: ListagemProdutoComponent },
  { path: 'carrinho', component: CarrinhoDeCompraComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
